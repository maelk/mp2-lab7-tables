#ifndef INCLUDE_TARRAYTABLE_H_
#define INCLUDE_TARRAYTABLE_H_

#include "TTable.h"
#include "TTabRecord.h"

enum TDataPos { FIRST_POS, CURRENT_POS, LAST_POS };

class TArrayTable : public TTable
{
protected:
	PTTabRecord *pRecs;				// ������ ��� ������� �������
	int TabSize;					// ����. ����� ������� � �������
	int CurrPos;					// ����� ������� ������

public:
	static const int TAB_MAX_SIZE = 25;
	TArrayTable(int size);
	~TArrayTable();
	
	// �������������� ������
	virtual bool IsFull() const { return DataCount >= TabSize; }
	int GetTabSize() const { return TabSize; }

	// ������
	virtual TKey GetKey() const { return GetKey(CURRENT_POS); }
	virtual PTDatValue GetValuePtr() const { return GetValuePtr(CURRENT_POS); }
	virtual TKey GetKey(TDataPos mode) const;
	virtual PTDatValue GetValuePtr(TDataPos mode) const;

	// ����������� ���� �����, ����������� ��� ����������� ������� � ������, ��� ����������� ���������
	PTTabRecord GetCurrRecord();

	// ���������
	virtual void Reset();
	virtual bool IsTabEnded() const;
	virtual Data GoNext();
	virtual Data SetCurrentPos(int pos);
	int GetCurrentPos() const;
	
	friend TSortTable;
};


#endif