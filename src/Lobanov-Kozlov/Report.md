﻿	# Лабораторная работа №7. Таблицы#

## Введение ##

Представление данных во многих задачах из разных областей человеческой деятельности может быть организовано при помощи таблиц. Таблицы представляют собой последовательности строк (записей), структура строк может быть различной, но обязательным является поле, задающее имя (ключ) записи. Таблицы применяются в бухгалтерском учете (ведомости заработной платы), в торговле (прайс-листы), в образовательных учреждениях (экзаменационные ведомости) и являются одними из наиболее распространенных структур данных, используемых при создании системного и прикладного математического обеспечения. Таблицы широко применяются в трансляторах (таблицы идентификаторов) и операционных системах, могут рассматриваться как программная реализация ассоциативной памяти и т.п. Существование отношения «иметь имя» является обязательным в большинстве разрабатываемых программистами структур данных; доступ по имени в этих структурах служит для получения соответствия между адресным принципом указания элементов памяти ЭВМ и общепринятым (более удобным для человека) способом указания объектов по их именам.

Целью лабораторной работы помимо изучения способов организации таблиц является начальное знакомство с принципами проектирования структур хранения, используемых в методах решения прикладных задач. На примере таблиц изучаются возможность выбора разных вариантов структур хранения, анализ их эффективности и определения областей приложений, в которых выбираемые структуры хранения являются наиболее эффективными.

В качестве практической задачи, на примере которой будут продемонстрированы возможные способы организации таблиц, рассматривается проблема статистической обработки результатов экзаменационной успеваемости студентов (выполнение таких, например, вычислений как определение среднего балла по предмету и/или по группе при назначении студентов на стипендию или при распределении студентов по кафедрам и т.п.).


##  Основные понятия и определения

**Таблица** (от лат. tabula – доска) – динамическая структура данных, базисным множеством которой является семейство линейных структур из записей (базисное отношение включения определяется операциями вставки и удаления записей).

**Запись** – кортеж, каждый элемент которого обычно именуется полем.

**Имя записи** (ключ) – одно из полей записи, по которому обычно осуществляется поиск записей в таблице; остальные поля образуют тело записи.

**Двоичное дерево поиска** – это представление данных в виде дерева, для которого выполняются условия:

- для любого узла (вершины) дерева существует не более двух потомков (двоичное дерево);
- для любого узла значения во всех узлах левого поддерева меньше значения в узле;
- для любого узла значения во всех узлах правого поддерева больше значения в узле.

**Хеш-функция**  – функция, ставящая в соответствие ключу номер записи в таблице (используется при организации таблиц с вычислимым входом).


## Требования к лабораторной работе

В рамках данной лабораторной работы ставится задача создания программных средств, поддерживающих табличные динамические структуры данных (таблицы) и базовые операции над ними:

-	поиск записи;
-	вставка записи (без дублирования);
-	удаление записи.

Выполнение операций над таблицами может осуществляться с различной степенью эффективности в зависимости от способа организации таблицы. 

В рамках лабораторной работы как показатель эффективности предлагается использовать количество операций, необходимых для выполнения операции поиска записи в таблице. Величина этого показателя должна определяться как аналитически (при использовании тех или иных упрощающих предположений), так и экспериментально на основе проведения вычислительных экспериментов.

В лабораторной работе предлагается реализовать следующие типы таблиц:

-	просмотровые (неупорядоченные);
-	упорядоченные (сортированные);
-	таблицы со структурами хранения на основе деревьев поиска;
-	хеш-таблицы или перемешанные (с вычисляемыми адресами).

Необходимо разработать интерфейс доступа к операциям поиска, вставки и удаления, не зависящий от способа организации таблицы.

### Используемые инструменты
- Система контроля версий **Git**.
- Фреймворк для написания автоматических тестов **Google Test**.
- Среда разработки **Microsoft Visual Studio 2015 Community Edition**.


## Структура проекта

При разработке классов используется ранее разработанный класс **TDatValue**.
 
TTabRecord.h – модуль с классом объектов-значений для записей таблицы;

```C++
#ifndef __TRECORD_H
#define __TRECORD_H

#include <iostream>
#include "TDatValue.h"

typedef std::string TKey;

class TTabRecord : public TDatValue {
protected:
	TKey Key; // Ключ записи
	PTDatValue pValue; // указатель на значение

public:
	TTabRecord(TKey k = "", PTDatValue pVal = NULL);
	TTabRecord(const TTabRecord&) = delete;
	void SetKey(TKey k);
	TKey GetKey() const;
	void SetValuePtr(PTDatValue p);
	PTDatValue GetValuePtr() const;
	virtual TDatValue* GetCopy();
	TTabRecord& operator=(TTabRecord& tr);
	virtual bool operator==(const TTabRecord& tr);
	virtual bool operator<(const TTabRecord& tr);
	virtual bool operator>(const TTabRecord& tr);

	friend class TArrayTable;
	friend class TScanTable;
	friend class TSortTable;
	friend class TTreeNode;
	friend class TArrayHash;
	friend class TListHash;

};

typedef TTabRecord* PTTabRecord;

#endif
```

TTabRecord.cpp

```C++
#include "../include/TTabRecord.h"

TTabRecord::TTabRecord(TKey k, PTDatValue pVal)
{
	Key = k;
	pValue = pVal;
}

void TTabRecord::SetKey(TKey k) {
	Key = k;
}

TKey TTabRecord::GetKey() const {
	return Key;
}

void TTabRecord::SetValuePtr(PTDatValue p) {
	pValue = p;
}

PTDatValue TTabRecord::GetValuePtr() const {
	return pValue;
}

TDatValue* TTabRecord::GetCopy()
{
	TDatValue* temp = new TTabRecord(Key, pValue);
	return temp;
}

TTabRecord& TTabRecord::operator=(TTabRecord& tr)
{
	Key = tr.Key;
	pValue = tr.pValue;
	return *this;
}

bool TTabRecord::operator==(const TTabRecord& tr)
{
	return Key == tr.Key;
}

bool TTabRecord::operator<(const TTabRecord& tr)
{
	return Key < tr.Key;
}

bool TTabRecord::operator>(const TTabRecord& tr)
{
	return Key > tr.Key;
}
```

TTable.h – абстрактный базовый класс, содержит спецификации методов таблицы;

```C++
#ifndef INCLUDE_TTABLE_H_
#define INCLUDE_TTABLE_H_

#include <string>

#include "TDataCom.h"
#include "TDatValue.h"

typedef std::string TKey;

class TTable : public TDataCom
{
protected:
	int DataCount;		// число записей в таблице
	int Efficiency;		// показатель эффективности выполнения операции

public:
	TTable() { DataCount = 0; Efficiency = 0; }
	virtual ~TTable() { };

	// информационные методы
	bool IsEmpty() const { return DataCount == 0; }
	virtual bool IsFull() const = 0;
	int GetDataCount() const { return DataCount; }
	int GetEfficiency() const { return Efficiency; }

	// доступ
	virtual TKey GetKey() const = 0;
	virtual PTDatValue GetValuePtr() const = 0;

	// навигация
	virtual void Reset() = 0;
	virtual bool IsTabEnded() const = 0;
	virtual Data GoNext() = 0;

	// Основные методы
	virtual PTDatValue FindRecord(TKey k) = 0;
	virtual void InsRecord(TKey k, PTDatValue pVal) = 0;
	virtual void DelRecord(TKey k) = 0;
};

#endif
```

TArrayTable.h – абстрактный базовый класс для таблиц с непрерывной памятью;

```C++
#ifndef INCLUDE_TARRAYTABLE_H_
#define INCLUDE_TARRAYTABLE_H_

#include "TTable.h"
#include "TTabRecord.h"

enum TDataPos { FIRST_POS, CURRENT_POS, LAST_POS };

class TArrayTable : public TTable
{
protected:
	PTTabRecord *pRecs;				// Память для записей таблицы
	int TabSize;					// макс. число записей в таблице
	int CurrPos;					// номер текущей записи

public:
	static const int TAB_MAX_SIZE = 25;
	TArrayTable(int size);
	~TArrayTable();
	
	// информационные методы
	virtual bool IsFull() const { return DataCount >= TabSize; }
	int GetTabSize() const { return TabSize; }

	// доступ
	virtual TKey GetKey() const { return GetKey(CURRENT_POS); }
	virtual PTDatValue GetValuePtr() const { return GetValuePtr(CURRENT_POS); }
	virtual TKey GetKey(TDataPos mode) const;
	virtual PTDatValue GetValuePtr(TDataPos mode) const;

	// добавленный мной метод, необходимый для возможности доступа к данным, для дальнейшего сравнения
	PTTabRecord GetCurrRecord();

	// навигация
	virtual void Reset();
	virtual bool IsTabEnded() const;
	virtual Data GoNext();
	virtual Data SetCurrentPos(int pos);
	int GetCurrentPos() const;
	
	friend TSortTable;
};


#endif
```

TArrayTable.cpp:

```C++
#include "../include//TArrayTable.h"

TArrayTable::TArrayTable(int size) {
	pRecs = new PTTabRecord[size];
	for (int i = 0; i < size; ++i) pRecs[i] = nullptr;
	TabSize = size;
	DataCount = CurrPos = 0;
}

TArrayTable::~TArrayTable()
{
	for (int i = 0; i < DataCount; ++i)
		delete pRecs[i];
	delete[] pRecs;
}

TKey TArrayTable::GetKey(TDataPos mode) const
{
	int pos = -1;
	switch (mode)
	{
	case TDataPos::FIRST_POS:
		pos = 0;
		break;
	case TDataPos::LAST_POS:
		pos = DataCount - 1;
		break;
	case TDataPos::CURRENT_POS:
		pos = CurrPos;
		break;
	}
	return (pos != -1) ? pRecs[pos]->Key : "";
}


PTDatValue TArrayTable::GetValuePtr(TDataPos mode) const
{
	int pos = -1;
	switch (mode)
	{
	case TDataPos::FIRST_POS:
		pos = 0;
		break;
	case TDataPos::LAST_POS:
		pos = DataCount - 1;
		break;
	case TDataPos::CURRENT_POS:
		pos = CurrPos;
		break;
	}
	return (pos != -1) ? pRecs[pos]->pValue : nullptr;
}

PTTabRecord TArrayTable::GetCurrRecord() {
	return pRecs[CurrPos];
}


void TArrayTable::Reset() {
	CurrPos = 0;
}

bool TArrayTable::IsTabEnded() const
{
	return CurrPos >= DataCount;
}

Data TArrayTable::GoNext()
{
	if (!IsTabEnded())
		CurrPos++;
	else
		SetRetCode(Data::INCORRECT_INCOMING);
	return GetRetCode();
}

Data TArrayTable::SetCurrentPos(int pos)
{
	if (pos < 0 || pos >= DataCount)
		SetRetCode(Data::INCORRECT_INCOMING);
	else
		CurrPos = pos;
	return GetRetCode();
}

int TArrayTable::GetCurrentPos() const
{
	return CurrPos;
}
```

TScanTable.h  – модуль с классом, обеспечивающим реализацию просматриваемых таблиц;

```C++
#ifndef INCLUDE_TSCANTABLE_H_
#define INCLUDE_TSCANTABLE_H_

#include "TArrayTable.h"

class TScanTable : public TArrayTable
{
public:
	TScanTable(int Size) : TArrayTable(Size) { }

	// Основные методы
	virtual PTDatValue FindRecord(TKey k);
	virtual void InsRecord(TKey k, PTDatValue pVal);
	virtual void DelRecord(TKey k);
};

#endif
```

TScanTable.cpp:

```C++
#include "../include/TScanTable.h"

PTDatValue TScanTable::FindRecord(TKey k)
{
	SetRetCode(Data::OK);

	for (int i = 0; i < TabSize; ++i) {
		if(pRecs[i] != nullptr) 
			if (pRecs[i]->GetKey() == k) {
				return pRecs[i]->GetValuePtr();
			}

	}
	SetRetCode(Data::NO_RECORD);
	return nullptr;
}

void TScanTable::InsRecord(TKey k, PTDatValue pVal)
{
	SetRetCode(Data::OUT_OF_RANGE);
	for (int i = 0; i < TabSize; ++i) {
		if (pRecs[i] == nullptr)
		{
			pRecs[i] = new TTabRecord(k, pVal);
			DataCount++;
			SetRetCode(Data::OK);
			break;
		}
	}
}

void TScanTable::DelRecord(TKey k)
{
	SetRetCode(Data::NO_RECORD);
	for (int i = 0; i < TabSize; i++) {
		if (pRecs[i] != nullptr)
			if (pRecs[i]->GetKey() == k) {
				delete pRecs[i];
				pRecs[i] = pRecs[DataCount - 1];
				pRecs[DataCount - 1] = nullptr;
				DataCount--;
				SetRetCode(Data::OK);
			}
	}
}
```

TSortTable.h  – модуль с классом, обеспечивающим реализацию упорядоченных таблиц;

```C++
#ifndef _T_SORT_TABLE_H_
#define _T_SORT_TABLE_H_

#include "TScanTable.h"

enum TSortMethod { INSERT_SORT, MERGE_SORT, QUICK_SORT };

class TSortTable : public TScanTable
{
protected:
	TSortMethod SortMethod;
	void SortData();
	void InsertSort(PTTabRecord *pMem, int DataCount);
	void MergeSort(PTTabRecord *pMem, int DataCount);
	void MergeSorter(PTTabRecord* &pData, PTTabRecord* &pBuf, int Size);
	void MergeData(PTTabRecord* &pData, PTTabRecord* &pBuff, int n1, int n2);
	void QuickSort(PTTabRecord *pMem, int DataCount);
	void QuickSplit(PTTabRecord *pData, int Size, int &Pivot);

public:
	TSortTable(int Size) : TScanTable(Size) { };
	TSortTable & operator=(const TScanTable &tab);
	TSortMethod GetSortMethod();
	void SetSortMethod(TSortMethod method);

	// основные методы
	virtual PTDatValue FindRecord(TKey k) override final;
	virtual void InsRecord(TKey k, PTDatValue pVal) override final;
	virtual void DelRecord(TKey k) override final;
};

#endif
```	

TSortTable.cpp:

```C++
#include "../include/TSortTable.h"

TSortTable & TSortTable::operator=(const TScanTable &tab)
{
	if (pRecs != nullptr) {
		for (int i = 0; i < DataCount; ++i)
			delete pRecs[i];
		delete[] pRecs;
		pRecs = nullptr;
	}
	TabSize = tab.TabSize;
	DataCount = tab.DataCount;
	pRecs = new PTTabRecord[TabSize];
	for (int i = 0; i < DataCount; ++i)
		pRecs[i] = (PTTabRecord)tab.pRecs[i]->GetCopy();
	SortData();
	CurrPos = 0;
	return *this;
}

PTDatValue TSortTable::FindRecord(TKey k)
{
	SetRetCode(Data::NO_RECORD);
	PTDatValue result = nullptr;
	if (DataCount > 0) {
		int i, i1 = 0, i2 = DataCount - 1;
		while (i1 <= i2) {
			i = (i1 + i2) / 2;
			if (pRecs[i]->GetKey() == k) {
				result = pRecs[i]->GetValuePtr();
				SetRetCode(Data::OK);
				break;
			}
			else if (pRecs[i]->GetKey() > k) {
				i2 = i - 1;
			}
			else {
				i1 = i + 1;
			}
		}
	}
	return result;
}

void TSortTable::InsRecord(TKey k, PTDatValue pVal)
{
	if (IsFull()) {
		SetRetCode(Data::FULL_TAB);
	}
	else {
		if (FindRecord(k) != nullptr) {
			SetRetCode(Data::DOUBLE_REC);
		}
		else {
			SetRetCode(Data::OK);
			for (int i = DataCount; i > CurrPos; i--)
				pRecs[i] = pRecs[i - 1];
			pRecs[CurrPos] = new TTabRecord(k, pVal);
			DataCount++;
			SortData();
		}
	}
}

void TSortTable::DelRecord(TKey k)
{
	SetRetCode(Data::NO_RECORD);
	for (int i = 0; i < DataCount; i++) {
		if (pRecs[i]->GetKey() == k) {
			delete pRecs[i];
			for (int j = i; j < DataCount; j++)
				pRecs[j] = pRecs[j + 1];
			DataCount--;
			SetRetCode(Data::OK);
		}
	}
}

TSortMethod TSortTable::GetSortMethod()
{
	return SortMethod;
}

void TSortTable::SetSortMethod(TSortMethod method)
{
	SortMethod = method;
}


void TSortTable::SortData()
{
	switch (SortMethod) {
	case TSortMethod::QUICK_SORT:
		QuickSort(pRecs, DataCount);
		break;
	case TSortMethod::INSERT_SORT:
		InsertSort(pRecs, DataCount);
		break;
	case TSortMethod::MERGE_SORT:
		QuickSort(pRecs, DataCount);
		break;
	default:
		InsertSort(pRecs, DataCount);
		break;
	}
}

void TSortTable::InsertSort(PTTabRecord *pMem, int DataCount)
{
	PTTabRecord pR;
	for (int i = 1, j; i < DataCount; i++)
	{
		pR = pRecs[i];
		for (j = i - 1; j >= 0; j--) {
			if (pRecs[j]->GetKey() > pR->GetKey()) {
				pRecs[j + 1] = pRecs[j];
			}
			else {
				break;
			}
		}
		pRecs[j + 1] = pR;
	}
}


void TSortTable::MergeSort(PTTabRecord * pMem, int DataCount)
{
	PTTabRecord *pData = pRecs;
	PTTabRecord *pBuff = new PTTabRecord[DataCount];
	PTTabRecord *pTemp = pBuff;
	MergeSorter(pData, pBuff, DataCount);
	if (pData == pTemp) {
		for (int i = 0; i < DataCount; i++)
			pBuff[i] = pData[i];
	}
	delete pTemp;
}

void TSortTable::MergeSorter(PTTabRecord *& pData, PTTabRecord *& pBuff, int Size)
{
	int n1 = (Size + 1) / 2;
	int n2 = Size - n1;
	if (Size > 2) {
		PTTabRecord *pDat2 = pData + n1, *pBuff2 = pBuff + n1;
		MergeSorter(pData, pBuff, n1);
		MergeSorter(pDat2, pBuff2, n2);
	}
	MergeData(pData, pBuff, n1, n2);
}

void TSortTable::MergeData(PTTabRecord *& pData, PTTabRecord *& pBuff, int n1, int n2)
{
	for (int i = 0; i < (n1 + n2); i++) {
		pBuff[i] = pData[i];
	}
	PTTabRecord *&tmp = pData;
	pData = pBuff;
	pBuff = tmp;
}

void TSortTable::QuickSort(PTTabRecord * pRecs, int DataCount)
{
	int pivot; // индекс ведущего элемента
	int n1, n2; // размеры разделенных блоков данных
	if (DataCount > 1) {
		QuickSplit(pRecs, DataCount, pivot);
		n1 = pivot + 1;
		n2 = DataCount - n1;
		QuickSort(pRecs, n1 - 1);
		QuickSort(pRecs+n1, n2);
	}
}

void TSortTable::QuickSplit(PTTabRecord * pData, int Size, int &Pivot)
{
	PTTabRecord pPivot = pData[0], pTemp;
	int i1 = 1, i2 = Size - 1;
	while (i1 <= i2) {
		while ((i1 < Size) && !(pData[i1]->GetKey() > pPivot->GetKey())) i1++;
		while (pData[i2]->GetKey() > pPivot->GetKey()) i2--;
		if (i1 < i2) {
			pTemp = pData[i1];
			pData[i1] = pData[i2];
			pData[i2] = pTemp;
		}
	}
	pData[0] = pData[i2];
	pData[i2] = pPivot;
	Pivot = i2;
}
```
TTreeNode.h  – модуль с абстрактным базовым классом объектов-значений для деревьев;

```C++
#ifndef TTREETABLE_H_
#define TTREETABLE_H_

#include <stack>
#include "TTable.h"
#include "TTreeNode.h"

class  TTreeTable : public TTable {
protected:
	PTTreeNode pRoot;			// указатель на корень дерева
	PTTreeNode *ppRef;			// адрес указателя на вершину-результата в FindRecord
	PTTreeNode pCurrent;		// указатель на текущую вершину
	int CurrPos;				// номер текущей вершины
	std::stack < PTTreeNode> St;		// стек для итератора
	void DeleteTreeTab(PTTreeNode pNode); // удаление
public:
	TTreeTable() : TTable() { CurrPos = 0; pRoot = pCurrent = NULL; ppRef = NULL; }
	~TTreeTable() { DeleteTreeTab(pRoot); }				// деструктор
	
	// информационные методы
	virtual bool IsFull() const override;						//заполнена?
	
	//основные методы
	virtual PTDatValue FindRecord(TKey k) override;				// найти запись
	virtual void InsRecord(TKey k, PTDatValue pVal) override; 	// вставить
	virtual void DelRecord(TKey k) override;					// удалить запись
	
	// навигация
	virtual TKey GetKey(void) const override;
	virtual PTDatValue GetValuePtr(void) const override;
	virtual void Reset(void) override;					// установить на первую запись
	virtual bool IsTabEnded(void) const override;		// таблица завершена?
	virtual Data GoNext(void) override;					// переход к следующей записи
};

#endif
```

TTreeNode.cpp:

```C++
#ifndef TTREETABLE_H_
#define TTREETABLE_H_

#include <stack>
#include "TTable.h"
#include "TTreeNode.h"

class  TTreeTable : public TTable {
protected:
	PTTreeNode pRoot;			// указатель на корень дерева
	PTTreeNode *ppRef;			// адрес указателя на вершину-результата в FindRecord
	PTTreeNode pCurrent;		// указатель на текущую вершину
	int CurrPos;				// номер текущей вершины
	std::stack < PTTreeNode> St;		// стек для итератора
	void DeleteTreeTab(PTTreeNode pNode); // удаление
public:
	TTreeTable() : TTable() { CurrPos = 0; pRoot = pCurrent = NULL; ppRef = NULL; }
	~TTreeTable() { DeleteTreeTab(pRoot); }				// деструктор
	
	// информационные методы
	virtual bool IsFull() const override;						//заполнена?
	
	//основные методы
	virtual PTDatValue FindRecord(TKey k) override;				// найти запись
	virtual void InsRecord(TKey k, PTDatValue pVal) override; 	// вставить
	virtual void DelRecord(TKey k) override;					// удалить запись
	
	// навигация
	virtual TKey GetKey(void) const override;
	virtual PTDatValue GetValuePtr(void) const override;
	virtual void Reset(void) override;					// установить на первую запись
	virtual bool IsTabEnded(void) const override;		// таблица завершена?
	virtual Data GoNext(void) override;					// переход к следующей записи
};

#endif
```

TTreeTable.h  – модуль с классом, реализующим таблицы в виде деревьев поиска;

```C++
#ifndef TTREETABLE_H_
#define TTREETABLE_H_

#include <stack>
#include "TTable.h"
#include "TTreeNode.h"

class  TTreeTable : public TTable {
protected:
	PTTreeNode pRoot;			// указатель на корень дерева
	PTTreeNode *ppRef;			// адрес указателя на вершину-результата в FindRecord
	PTTreeNode pCurrent;		// указатель на текущую вершину
	int CurrPos;				// номер текущей вершины
	std::stack < PTTreeNode> St;		// стек для итератора
	void DeleteTreeTab(PTTreeNode pNode); // удаление
public:
	TTreeTable() : TTable() { CurrPos = 0; pRoot = pCurrent = NULL; ppRef = NULL; }
	~TTreeTable() { DeleteTreeTab(pRoot); }				// деструктор
	
	// информационные методы
	virtual bool IsFull() const override;						//заполнена?
	
	//основные методы
	virtual PTDatValue FindRecord(TKey k) override;				// найти запись
	virtual void InsRecord(TKey k, PTDatValue pVal) override; 	// вставить
	virtual void DelRecord(TKey k) override;					// удалить запись
	
	// навигация
	virtual TKey GetKey(void) const override;
	virtual PTDatValue GetValuePtr(void) const override;
	virtual void Reset(void) override;					// установить на первую запись
	virtual bool IsTabEnded(void) const override;		// таблица завершена?
	virtual Data GoNext(void) override;					// переход к следующей записи
};

#endif
```

TTreeTable.cpp:

```C++
#include "../include/TTreeTable.h"

bool TTreeTable::IsFull() const
{
	return false;
}

PTDatValue TTreeTable::FindRecord(TKey k) {
	PTTreeNode tmp = pRoot;
	ppRef = &pRoot;
	while (tmp != nullptr) {
		if (tmp->GetKey() == k) break;
		if (tmp->GetKey() < k) ppRef = &tmp->pRight;
		else ppRef = &tmp->pLeft;
		tmp = *ppRef;
	}
	if (tmp == nullptr) {
		SetRetCode(Data::NO_RECORD);
		return nullptr;
	}
	else {
		SetRetCode(Data::OK);
		return tmp->GetValuePtr();
	}
}

void TTreeTable::InsRecord(TKey k, PTDatValue pVal)
{
	if (IsFull()) {
		SetRetCode(Data::FULL_TAB);
	}
	else {
		if (FindRecord(k) != nullptr) {
			SetRetCode(Data::DOUBLE_REC);
		}
		else {
			*ppRef = new TTreeNode(k, pVal);
			DataCount++;
		}
	}
}

void TTreeTable::DelRecord(TKey k)
{
	if (FindRecord(k) == nullptr) {
		SetRetCode(Data::NO_RECORD);
	}
	else {
		PTTreeNode tmp = pRoot;

		while (!St.empty())
			St.pop();
		while (tmp->GetKey() != k) {
			St.push(tmp);
			if (tmp->GetKey() < k)
				tmp = tmp->GetRight();
			else
				tmp = tmp->GetLeft();
		}
		// удаление листа
		if ((tmp->pLeft == nullptr) && (tmp->pRight == nullptr)) {
			if (!St.empty()) {
				PTTreeNode prev = St.top();
				if (prev != nullptr) {
					if (prev->GetRight() == tmp)
						prev->pRight = nullptr;
					else if (prev->GetLeft() == tmp)
						prev->pLeft = nullptr;
				}
			}
			else {
				pRoot = nullptr;
			}
			delete tmp;
			DataCount--;
		}
		// удаление звена с одним потомком (справа)
		else if (tmp->pLeft == nullptr) {
			if (!St.empty()) {
				PTTreeNode prev = St.top();
				if (prev != nullptr) {
					if (prev->GetRight() == tmp)
						prev->pRight = tmp->pRight;
					if (prev->GetLeft() == tmp)
						prev->pLeft = tmp->pRight;
				}
			}
			else {
				pRoot = tmp->GetRight();
			}
			delete tmp;
			DataCount--;
		}
		// удаление звена с одним потомком (слева)
		else if (tmp->pRight == nullptr) {
			if (!St.empty()) {
				PTTreeNode prev = St.top();
				if (prev != nullptr) {
					if (prev->GetRight() == tmp)
						prev->pRight = tmp->pLeft;
					if (prev->GetLeft() == tmp)
						prev->pLeft = tmp->pLeft;
				}
			}
			else {
				pRoot = tmp->GetLeft();
			}
			delete tmp;
			DataCount--;
		}
		// удаление звена с двумя потомками
		else {
			PTTreeNode down_left = tmp->GetRight();
			while (down_left->GetLeft() != nullptr)
				down_left = down_left->pLeft;
			down_left->pLeft = tmp->GetLeft();

			if (!St.empty()) {
				PTTreeNode prev = St.top();
				if (prev != nullptr) {
					if (prev->GetRight() == tmp)
						prev->pRight = tmp->pRight;
					if (prev->GetLeft() == tmp)
						prev->pLeft = tmp->pRight;
				}
			}
			else {
				pRoot = tmp->GetRight();
			}
			delete tmp;
			DataCount--;
		}

	}
}

TKey TTreeTable::GetKey(void) const
{
	return (pCurrent == nullptr) ? "" : pCurrent->GetKey();
}

PTDatValue TTreeTable::GetValuePtr(void) const
{
	return (pCurrent == nullptr) ? nullptr : pCurrent->GetValuePtr();
}

void TTreeTable::DeleteTreeTab(PTTreeNode pNode)
{
	if (pNode != nullptr) {
		DeleteTreeTab(pNode->GetLeft());
		DeleteTreeTab(pNode->GetRight());
		delete pNode;
	}
}

void TTreeTable::Reset(void)
{
	PTTreeNode pNode = pCurrent = pRoot;
	CurrPos = 0;
	while (pNode != nullptr) {
		St.push(pNode);
		pCurrent = pNode;
		pNode = pNode->GetLeft();
	}
	SetRetCode(Data::OK);
}

bool TTreeTable::IsTabEnded(void) const
{
	return (CurrPos >= DataCount);
}

Data TTreeTable::GoNext(void)
{
	CurrPos++;
	if (!IsTabEnded() && (pCurrent != nullptr)) {
		PTTreeNode pNode = pCurrent = pCurrent->GetRight();
		St.pop();
		while (pNode != nullptr) {
			St.push(pNode);
			pCurrent = pNode;
			pNode = pNode->GetLeft();
		}
		if ((pCurrent == nullptr) && !St.empty())
			pCurrent = St.top();

	}
	else SetRetCode(Data::OUT_OF_RANGE);
	return GetRetCode();
}
```
TBalanceNode.h  – модуль с базовым классом объектов-значений для сбалансированных деревьев;

```C++
#ifndef TBALANCENODE_H_
#define TBALANCENODE_H_

#include "TTreeNode.h"

enum class Bal { BalOK, BalLeft, BalRight };

class  TBalanceNode : public TTreeNode {
protected:
	Bal Balance; // индекс балансировки вершины
public:
	TBalanceNode(TKey k = "", PTDatValue pVal = NULL, PTTreeNode pL = NULL,
		PTTreeNode pR = NULL, Bal bal = Bal::BalOK) : TTreeNode(k, pVal, pL, pR),
		Balance(bal) {};			// конструктор
	virtual TDatValue * GetCopy();  // изготовить копию
	Bal GetBalance(void) const;
	void SetBalance(Bal bal);
	friend class TBalanceTree;
};

typedef TBalanceNode *PTBalanceNode;

#endif
```	

```C++
#include "../include/TBalanceNode.h"

TDatValue * TBalanceNode::GetCopy()
{
	TBalanceNode *tmp = new TBalanceNode(Key, pValue, nullptr, nullptr, Bal::BalOK);
	return tmp;
}

Bal TBalanceNode::GetBalance(void) const
{
	return Balance;
}

void TBalanceNode::SetBalance(Bal bal)
{
	Balance = bal;
}
```
TBalanceTree.h – модуль с классом, реализующим таблицы в виде сбалансированных деревьев поиска;


```C++
#ifndef TBALANCETREE_H_
#define TBALANCETREE_H_

#include "TTreeTable.h"
#include "TBalanceNode.h"

enum class Height { OK, Inc };

class  TBalanceTree : public TTreeTable {
protected:
	Height InsBalanceTree(PTBalanceNode &pNode, TKey k, PTDatValue pVal);
	Height LeftTreeBalancing(PTBalanceNode &pNode);  // баланс. левого поддерева
	Height RightTreeBalancing(PTBalanceNode &pNode); // баланс. правого поддерева
public:
	TBalanceTree() :TTreeTable() {} // конструктор
	//основные методы
	virtual void InsRecord(TKey k, PTDatValue pVal) override final; // вставить
	virtual void DelRecord(TKey k) override final;                  // удалить
};

typedef TBalanceTree *PTBalanceTree;

#endif
```

TBalanceTree.cpp:

```C++
#include "../include/TBalanceTree.h"

Height TBalanceTree::InsBalanceTree(PTBalanceNode &pNode, TKey k, PTDatValue pVal)
{
	Height HeightIndex = Height::OK;
	if (pNode == nullptr) { // вставка вершины
		pNode = new TBalanceNode(k, pVal);
		HeightIndex = Height::Inc;
		DataCount++;
	}
	else if (k < pNode->GetKey()) {
		if (InsBalanceTree((PTBalanceNode&)pNode->pLeft, k, pVal) == Height::Inc) {
			HeightIndex = LeftTreeBalancing(pNode);
		}
	}
	else if (k > pNode->GetKey()) {
		if (InsBalanceTree((PTBalanceNode&)pNode->pRight, k, pVal) == Height::Inc) {
			HeightIndex = RightTreeBalancing(pNode);
		}
	}
	else {
		SetRetCode(Data::DOUBLE_REC);
		HeightIndex = Height::OK;
	}
	return HeightIndex;
}

Height TBalanceTree::LeftTreeBalancing(PTBalanceNode &pNode)
{
	Height HeightIndex = Height::OK;
	switch (pNode->GetBalance()) {
	case Bal::BalRight:
		pNode->SetBalance(Bal::BalOK);
		HeightIndex = Height::OK;
		break;
	case Bal::BalOK:
		pNode->SetBalance(Bal::BalLeft);
		HeightIndex = Height::Inc;
		break;
	case Bal::BalLeft:
		PTBalanceNode p1, p2;
		p1 = PTBalanceNode(pNode->GetLeft());
		if (p1->GetBalance() == Bal::BalLeft) {
			pNode->pLeft = p1->pRight;
			p1->pRight = pNode;
			pNode->SetBalance(Bal::BalOK);
			pNode = p1;
		}
		else {
			p2 = PTBalanceNode(p1->GetRight());
			p1->pRight = p2->pLeft;
			p2->pLeft = p1;
			pNode->pLeft = p2->pRight;
			p2->pRight = pNode;
			if (p2->GetBalance() == Bal::BalLeft) {
				pNode->SetBalance(Bal::BalRight);
			}
			else {
				pNode->SetBalance(Bal::BalOK);
			}
			if (p2->GetBalance() == Bal::BalRight) {
				p1->SetBalance(Bal::BalLeft);
			}
			else {
				p1->SetBalance(Bal::BalOK);
			}
			pNode = p2;
		}
		pNode->SetBalance(Bal::BalOK);
		HeightIndex = Height::OK;
	}
	return HeightIndex;
}

Height TBalanceTree::RightTreeBalancing(PTBalanceNode &pNode)
{
	Height HeightIndex = Height::OK;
	switch (pNode->GetBalance()) {
	case Bal::BalLeft:
		pNode->SetBalance(Bal::BalOK);
		HeightIndex = Height::OK;
		break;
	case Bal::BalOK:
		pNode->SetBalance(Bal::BalRight);
		HeightIndex = Height::Inc;
		break;
	case Bal::BalRight:
		PTBalanceNode p1, p2;
		p1 = PTBalanceNode(pNode->GetRight());
		if (p1->GetBalance() == Bal::BalRight) {
			pNode->pRight = p1->pLeft;
			p1->pLeft = pNode;
			pNode->SetBalance(Bal::BalOK);
			pNode = p1;
		}
		else {
			p2 = PTBalanceNode(p1->GetLeft());
			p1->pLeft = p2->pRight;
			p2->pRight = p1;
			pNode->pRight = p2->pLeft;
			p2->pLeft = pNode;
			if (p2->GetBalance() == Bal::BalRight) {
				pNode->SetBalance(Bal::BalLeft);
			}
			else {
				pNode->SetBalance(Bal::BalOK);
			}
			if (p2->GetBalance() == Bal::BalLeft) {
				p1->SetBalance(Bal::BalRight);
			}
			else {
				p1->SetBalance(Bal::BalOK);
			}
			pNode = p2;
		}
		pNode->SetBalance(Bal::BalOK);
		HeightIndex = Height::OK;
	}
	return HeightIndex;
}

void TBalanceTree::InsRecord(TKey k, PTDatValue pVal)
{
	if (IsFull()) {
		SetRetCode(Data::FULL_TAB);
	}
	else {
		InsBalanceTree((PTBalanceNode&)pRoot, k, pVal);
	}
}

void TBalanceTree::DelRecord(TKey k)
{
	if (FindRecord(k) == nullptr) {
		SetRetCode(Data::NO_RECORD);
	}
	else {
		PTTreeNode tmp = pRoot;

		while (!St.empty())
			St.pop();
		while (tmp->GetKey() != k) {
			St.push(tmp);
			if (tmp->GetKey() < k)
				tmp = tmp->GetRight();
			else
				tmp = tmp->GetLeft();
		}

		TKey k2 = tmp->GetKey();
		// удаление листа
		if ((tmp->pLeft == nullptr) && (tmp->pRight == nullptr)) {
			if (!St.empty()) {
				PTTreeNode prev = St.top();
				if (prev != nullptr) {
					if (prev->GetRight() == tmp)
						prev->pRight = nullptr;
					if (prev->GetLeft() == tmp)
						prev->pLeft = nullptr;
				}
			}
			else {
				pRoot = nullptr;
			}
			delete tmp;
			DataCount--;
		}
		// удаление звена с одним потомком (справа)
		else if (tmp->pLeft == nullptr) {
			if (!St.empty()) {
				PTTreeNode prev = St.top();
				if (prev != nullptr) {
					if (prev->GetRight() == tmp)
						prev->pRight = tmp->pRight;
					if (prev->GetLeft() == tmp)
						prev->pLeft = tmp->pRight;
				}
			}
			else {
				pRoot = tmp->GetRight();
			}
			delete tmp;
			DataCount--;
		}
		// удаление звена с одним потомком (слева)
		else if (tmp->pRight == nullptr) {
			if (!St.empty()) {
				PTTreeNode prev = St.top();
				if (prev != nullptr) {
					if (prev->GetRight() == tmp)
						prev->pRight = tmp->pLeft;
					if (prev->GetLeft() == tmp)
						prev->pLeft = tmp->pLeft;
				}
			}
			else {
				pRoot = tmp->GetLeft();
			}
			delete tmp;
			DataCount--;
		}
		// удаление звена с двумя потомками
		else {
			PTTreeNode down_left = tmp->GetRight();
			while (down_left->GetLeft() != nullptr)
				down_left = down_left->pLeft;
			down_left->pLeft = tmp->GetLeft();

			if (!St.empty()) {
				PTTreeNode prev = St.top();
				if (prev != nullptr) {
					if (prev->GetRight() == tmp)
						prev->pRight = tmp->pRight;
					if (prev->GetLeft() == tmp)
						prev->pLeft = tmp->pRight;
				}
			}
			else {
				pRoot = tmp->GetRight();
			}
			delete tmp;
			DataCount--;
		}
		if (pRoot != nullptr) {
			if (k2 < pRoot->GetKey()) {
				LeftTreeBalancing((PTBalanceNode&)pRoot);
			}
			else  if (k2 > pRoot->GetKey()) {
				RightTreeBalancing((PTBalanceNode&)pRoot);
			}
		}
	}
}
```

### Проверка работоспособности при помощи Google Test Framework


test_tables.cpp:

```C++

#include "../gtest/gtest.h"
#include "../include/TSortTable.h"
#include "../include/TTreeTable.h"
#include "../include/TBalanceTree.h"

TEST(TScanTable, can_create_ScanTable) {
	EXPECT_NO_FATAL_FAILURE(TScanTable table(5));
}

TEST(TSortTable, can_create_SortTable) {
	EXPECT_NO_FATAL_FAILURE(TSortTable table(5));
}

TEST(TTreeNode, can_create_TTreeNode) {
	EXPECT_NO_FATAL_FAILURE(TTreeNode node());
}

TEST(TBalanceNode, can_created_TBalanceNode) {
	EXPECT_NO_FATAL_FAILURE(TBalanceNode TBal);
}

TEST(TTreeTable, can_create_TTreeTable) {
	EXPECT_NO_FATAL_FAILURE(TTreeTable table);
}

TEST(TBalanceTree, can_create_TBalanceTree) {
	EXPECT_NO_FATAL_FAILURE(TBalanceTree tree);
}

TEST(TTabRecord, can_get_key) {
	TTabRecord tab("test");
	EXPECT_EQ(tab.GetKey(), "test");
}

TEST(TTabRecord, can_get_value) {
	TDatValue* tmp = new TTabRecord("1234", NULL);
	TTabRecord tab("test", tmp);
	EXPECT_EQ(tab.GetValuePtr(), tmp);
}

TEST(TTabRecord, can_set_value) {
	TDatValue* tmp = new TTabRecord("1234", NULL);
	TDatValue* exp = new TTabRecord("4321", NULL);
	TTabRecord tab("test", tmp);
	tab.SetValuePtr(exp);
	EXPECT_EQ(tab.GetValuePtr(), exp);	
}

TEST(TTabRecord, can_set_key) {
	TDatValue* tmp = new TTabRecord("1234", NULL);
	TTabRecord tab("test", tmp);
	tab.SetKey("EXPECT");
	EXPECT_EQ(tab.GetKey(), "EXPECT");
}

TEST(TScanTable, can_ins_record) {
	TScanTable* table = new TScanTable(5);
	int i = 5;
	TTabRecord rec("Kozlov I.", (TDatValue*)&i);
	table->InsRecord("Kozlov I.", (TDatValue*)&i);
	EXPECT_TRUE(rec == *table->GetCurrRecord());
}

TEST(TScanTable, can_find_record) {
	TScanTable* table = new TScanTable(4);
	int* i = new int(1);
	i[0] = 1;
	table->InsRecord("Lojkin B.", (TDatValue*)&i[0]);
	i = new int(1);
	i[0] = 2;
	table->InsRecord("Grachev I.", (TDatValue*)&i[0]);
	i = new int(1);
	i[0] = 3;
	table->InsRecord("Smirnov O.", (TDatValue*)i);
	i = new int(1);
	i[0] = 4;
	table->InsRecord("Rakuwkin A.", (TDatValue*)i);

	EXPECT_EQ(2, *((int*)(table->FindRecord("Grachev I."))));
}

TEST(TScanTable, Can_del_record) {
	TScanTable* table = new TScanTable(4);
	int* i = new int(1);
	i[0] = 1;
	table->InsRecord("Lojkin B.", (TDatValue*)&i[0]);
	i = new int(1);
	i[0] = 2;
	table->InsRecord("Grachev I.", (TDatValue*)&i[0]);
	i = new int(1);
	i[0] = 3;
	table->InsRecord("Smirnov O.", (TDatValue*)i);
	i = new int(1);
	i[0] = 4;
	table->InsRecord("Rakuwkin A.", (TDatValue*)i);

	EXPECT_NO_FATAL_FAILURE(table->DelRecord("Mamai"));
	EXPECT_TRUE(nullptr == table->FindRecord("Mamai"));
}

TEST(TSortTable, can_ins_record) {
	TSortTable* table = new TSortTable(1);
	int i = 1;
	TTabRecord rec("Kozlov I.", (TDatValue*)&i);
	table->InsRecord("Kozlov I.", (PTDatValue)&i);

	EXPECT_TRUE(rec == *table->GetCurrRecord());
}

TEST(TSortTable, can_assignment_table) {
	TSortTable* table = new TSortTable(1);
	int i = 1;
	table->InsRecord("Kozlov I.", (PTDatValue)&i);
	TSortTable table2(2);

	table2 = *table;

	EXPECT_TRUE(table2.GetCurrRecord(), table->GetCurrRecord());
}

TEST(TSortTable, can_set_and_get_sort_metod) {
	TSortTable* table = new TSortTable(5);
	int i = 5;
	table->SetSortMethod(INSERT_SORT);

	EXPECT_TRUE(table->GetSortMethod() == INSERT_SORT);
}

TEST(TSortTable, can_used_all_sort___can_find_rec) {
	TSortTable* table = new TSortTable(5);
	int* i = new int(1);
	i[0] = 5;
	table->SetSortMethod(INSERT_SORT);
	EXPECT_NO_FATAL_FAILURE(table->InsRecord("Lojkin A.", (TDatValue*)&i[0]));
	i = new int(1);
	i[0] = 4;
	table->SetSortMethod(MERGE_SORT);
	EXPECT_NO_FATAL_FAILURE(table->InsRecord("Nikiforov N.", (TDatValue*)&i[0]));
	i = new int(1);
	i[0] = 3;
	table->SetSortMethod(QUICK_SORT);
	EXPECT_NO_FATAL_FAILURE(table->InsRecord("Smirnov O.", (TDatValue*)&i[0]));
	i = new int(1);
	i[0] = 4;
	table->SetSortMethod(INSERT_SORT);
	EXPECT_NO_FATAL_FAILURE(table->InsRecord("Bukin G.", (TDatValue*)&i[0]));

	EXPECT_EQ(4, *((int*)(table->FindRecord("Nikiforov N."))));
}


TEST(TTreeNode, can_get_tree_branch) {
	int* i = new int(1);
	i[0] = 5;
	TTreeNode left("Lojkin V.", (PTDatValue)&i[0]);
	i = new int(1);
	i[0] = 4;
	TTreeNode right("Borisov G.", (PTDatValue)&i[0]);
	i = new int(1);
	i[0] = 10;
	TTreeNode Center("Prohorov A.", (PTDatValue)&i[0], &left, &right);

	EXPECT_EQ(Center.GetLeft(), &left);
	EXPECT_EQ(Center.GetRight(), &right);
}

TEST(TTreeTable, can_ins_rec) {
	int* i = new int(1);
	i[0] = 3;
	TTreeTable* table = new TTreeTable();
	EXPECT_NO_FATAL_FAILURE(table->InsRecord("Nemcov A.", (PTDatValue)&i[0]));
}

TEST(TTreeTable, can_find_rec) {
	int* i = new int(1);
	i[0] = 3;
	TTreeTable* table = new TTreeTable();
	table->InsRecord("just", (PTDatValue)&i[0]);
	i = new int(1);
	i[0] = 5;
	table->InsRecord("do", (PTDatValue)&i[0]);
	i = new int(1);
	i[0] = 2;
	table->InsRecord("it", (PTDatValue)&i[0]);

	EXPECT_EQ(*(int*)table->FindRecord("do"), 5);
}

TEST(TBalanceTree, can_add_and_find_rec) {
	int* i = new int(1);
	i[0] = 3;
	TBalanceTree* table = new TBalanceTree();
	table->InsRecord("keep", (PTDatValue)&i[0]);
	i = new int(1);
	i[0] = 5;
	table->InsRecord("calm", (PTDatValue)&i[0]);
	i = new int(1);
	i[0] = 1;
	table->InsRecord("and", (PTDatValue)&i[0]);
	i = new int(1);
	i[0] = 2;
	table->InsRecord("doing", (PTDatValue)&i[0]);
	i = new int(1);
	i[0] = 4;
	table->InsRecord("work", (PTDatValue)&i[0]);

	EXPECT_EQ(*(int*)table->FindRecord("work"), 4);
}
```
![Результат работы тестов](testimage.png)

##**Вывод**##

В ходе выполнения данной работы мы ознакомились с различными структурами хранения, которые были использованны для реализации нескольких типов таблиц: просмотровые (неупорядоченные), упорядоченные (сортированные),таблицы со структурами хранения на основе деревьев поиска. 
Получили навыки создания таблиц и организации работы с ними, включая доступ к элементам таблиц, поиск нужного элемента по ключу для разных типов таблиц.
Для проверки работоспособности написанной системы был использован Google Test Framework.