#include "../gtest/gtest.h"
#include "../include/TSortTable.h"
#include "../include/TTreeTable.h"
#include "../include/TBalanceTree.h"

TEST(TScanTable, can_create_ScanTable) {
	EXPECT_NO_FATAL_FAILURE(TScanTable table(5));
}

TEST(TSortTable, can_create_SortTable) {
	EXPECT_NO_FATAL_FAILURE(TSortTable table(5));
}

TEST(TTreeNode, can_create_TTreeNode) {
	EXPECT_NO_FATAL_FAILURE(TTreeNode node());
}

TEST(TBalanceNode, can_created_TBalanceNode) {
	EXPECT_NO_FATAL_FAILURE(TBalanceNode TBal);
}

TEST(TTreeTable, can_create_TTreeTable) {
	EXPECT_NO_FATAL_FAILURE(TTreeTable table);
}

TEST(TBalanceTree, can_create_TBalanceTree) {
	EXPECT_NO_FATAL_FAILURE(TBalanceTree tree);
}

TEST(TTabRecord, can_get_key) {
	TTabRecord tab("test");
	EXPECT_EQ(tab.GetKey(), "test");
}

TEST(TTabRecord, can_get_value) {
	TDatValue* tmp = new TTabRecord("1234", NULL);
	TTabRecord tab("test", tmp);
	EXPECT_EQ(tab.GetValuePtr(), tmp);
}

TEST(TTabRecord, can_set_value) {
	TDatValue* tmp = new TTabRecord("1234", NULL);
	TDatValue* exp = new TTabRecord("4321", NULL);
	TTabRecord tab("test", tmp);
	tab.SetValuePtr(exp);
	EXPECT_EQ(tab.GetValuePtr(), exp);	
}

TEST(TTabRecord, can_set_key) {
	TDatValue* tmp = new TTabRecord("1234", NULL);
	TTabRecord tab("test", tmp);
	tab.SetKey("EXPECT");
	EXPECT_EQ(tab.GetKey(), "EXPECT");
}

TEST(TScanTable, can_ins_record) {
	TScanTable* table = new TScanTable(5);
	int i = 5;
	TTabRecord rec("Kozlov I.", (TDatValue*)&i);
	table->InsRecord("Kozlov I.", (TDatValue*)&i);
	EXPECT_TRUE(rec == *table->GetCurrRecord());
}

TEST(TScanTable, can_find_record) {
	TScanTable* table = new TScanTable(4);
	int* i = new int(1);
	i[0] = 1;
	table->InsRecord("Lojkin B.", (TDatValue*)&i[0]);
	i = new int(1);
	i[0] = 2;
	table->InsRecord("Grachev I.", (TDatValue*)&i[0]);
	i = new int(1);
	i[0] = 3;
	table->InsRecord("Smirnov O.", (TDatValue*)i);
	i = new int(1);
	i[0] = 4;
	table->InsRecord("Rakuwkin A.", (TDatValue*)i);

	EXPECT_EQ(2, *((int*)(table->FindRecord("Grachev I."))));
}

TEST(TScanTable, Can_del_record) {
	TScanTable* table = new TScanTable(4);
	int* i = new int(1);
	i[0] = 1;
	table->InsRecord("Lojkin B.", (TDatValue*)&i[0]);
	i = new int(1);
	i[0] = 2;
	table->InsRecord("Grachev I.", (TDatValue*)&i[0]);
	i = new int(1);
	i[0] = 3;
	table->InsRecord("Smirnov O.", (TDatValue*)i);
	i = new int(1);
	i[0] = 4;
	table->InsRecord("Rakuwkin A.", (TDatValue*)i);

	EXPECT_NO_FATAL_FAILURE(table->DelRecord("Mamai"));
	EXPECT_TRUE(nullptr == table->FindRecord("Mamai"));
}

TEST(TSortTable, can_ins_record) {
	TSortTable* table = new TSortTable(1);
	int i = 1;
	TTabRecord rec("Kozlov I.", (TDatValue*)&i);
	table->InsRecord("Kozlov I.", (PTDatValue)&i);

	EXPECT_TRUE(rec == *table->GetCurrRecord());
}

TEST(TSortTable, can_assignment_table) {
	TSortTable* table = new TSortTable(1);
	int i = 1;
	table->InsRecord("Kozlov I.", (PTDatValue)&i);
	TSortTable table2(2);

	table2 = *table;

	EXPECT_TRUE(table2.GetCurrRecord(), table->GetCurrRecord());
}

TEST(TSortTable, can_set_and_get_sort_metod) {
	TSortTable* table = new TSortTable(5);
	int i = 5;
	table->SetSortMethod(INSERT_SORT);

	EXPECT_TRUE(table->GetSortMethod() == INSERT_SORT);
}

TEST(TSortTable, can_used_all_sort_can_find_rec) {
	TSortTable* table = new TSortTable(5);
	int* i = new int(1);
	i[0] = 5;
	table->SetSortMethod(INSERT_SORT);
	EXPECT_NO_FATAL_FAILURE(table->InsRecord("Lojkin A.", (TDatValue*)&i[0]));
	i = new int(1);
	i[0] = 4;
	table->SetSortMethod(MERGE_SORT);
	EXPECT_NO_FATAL_FAILURE(table->InsRecord("Nikiforov N.", (TDatValue*)&i[0]));
	i = new int(1);
	i[0] = 3;
	table->SetSortMethod(QUICK_SORT);
	EXPECT_NO_FATAL_FAILURE(table->InsRecord("Smirnov O.", (TDatValue*)&i[0]));
	i = new int(1);
	i[0] = 4;
	table->SetSortMethod(INSERT_SORT);
	EXPECT_NO_FATAL_FAILURE(table->InsRecord("Bukin G.", (TDatValue*)&i[0]));

	EXPECT_EQ(4, *((int*)(table->FindRecord("Nikiforov N."))));
}


TEST(TTreeNode, can_get_tree_branch) {
	int* i = new int(1);
	i[0] = 5;
	TTreeNode left("Lojkin V.", (PTDatValue)&i[0]);
	i = new int(1);
	i[0] = 4;
	TTreeNode right("Borisov G.", (PTDatValue)&i[0]);
	i = new int(1);
	i[0] = 10;
	TTreeNode Center("Prohorov A.", (PTDatValue)&i[0], &left, &right);

	EXPECT_EQ(Center.GetLeft(), &left);
	EXPECT_EQ(Center.GetRight(), &right);
}
